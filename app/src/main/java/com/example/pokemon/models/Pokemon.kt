package com.example.pokemon.models

import com.google.gson.annotations.SerializedName

data class Pokemon (
    @SerializedName("id") val id:Int,
    @SerializedName("name") val name:String,
    @SerializedName("height") val height:Int,
    @SerializedName("weight") val weight:Int,
    @SerializedName("sprite") val sprite:String
)