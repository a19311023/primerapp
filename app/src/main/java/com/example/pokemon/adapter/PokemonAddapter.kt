package com.example.pokemon.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pokemon.R
import com.example.pokemon.models.Pokemon



class PokemonAddapter(private val pokeList: Array<Pokemon>): RecyclerView.Adapter<PokemonViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {

        val layoutInflater = LayoutInflater.from(parent?.context)
        val gridItem = layoutInflater.inflate(R.layout.pokemon_item, parent, false)
        return PokemonViewHolder(gridItem)
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {

        holder.bindItems(pokeList[position])
    }

    override fun getItemCount(): Int {
        return pokeList.count()
    }
}

class PokemonViewHolder(view: View): RecyclerView.ViewHolder(view){

    fun bindItems(pokemon: Pokemon){

        try {

           // itemView.item_pokemon_number.text = pokemon.id.toString()
             //itemView.item_pokemon_name.text = pokemon.name
            //view.item_pokemon_sprite.text = pokemon.sprite

        }catch (e:Exception){
            Log.v("pokemonAdapter", e.message.toString())
        }
    }
}