package com.example.pokemon.network

import com.example.pokemon.models.Pokemon
import retrofit2.Response
import retrofit2.http.GET


interface PokeApiClient {

    @GET("pokemon")
    suspend fun getAllPokemon(): Response<List<Pokemon>>

}