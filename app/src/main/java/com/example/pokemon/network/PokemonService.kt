package com.example.pokemon.network

import com.example.pokemon.core.RetrofitClient
import com.example.pokemon.models.Pokemon
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PokemonService {

    private val retrofit = RetrofitClient.getRetrofit()

    suspend fun getPokemon(): List<Pokemon> {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(PokeApiClient::class.java).getAllPokemon()
            response.body()?: emptyList()
        }

    }
}
