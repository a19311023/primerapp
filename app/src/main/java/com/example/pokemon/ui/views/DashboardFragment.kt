package com.example.pokemon.ui.views

import android.os.Bundle
import android.os.Message
import android.telecom.Call
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pokemon.R
import com.example.pokemon.adapter.PokemonAddapter
import com.example.pokemon.databinding.FragmentDashboardBinding
import com.example.pokemon.models.Pokemon
import com.example.pokemon.network.PokemonService
import retrofit2.Response
import javax.security.auth.callback.Callback

class DashboardFragment : Fragment() {

    private lateinit var adapter: PokemonAddapter
    private lateinit var Pokelist: List<Pokemon>
    private lateinit var  PokeService : PokemonService

    private var _binding: FragmentDashboardBinding? = null

    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        getPokemonData()
        return binding.root
    }

    fun getPokemonData() {

        val call = PokeService.getPokemon()

        call.enqueue(object : Callback<List<Pokemon>>) {
            fun onResponse(
                call: Call<List<Pokemon>>?,
                response: Response<List<Pokemon>>
            ) {
                Pokelist = response.body()!!
            }

            fun onFailure(call: Call<Message?>, t: Throwable) {

                Toast.makeText(this, "Error" + t.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun setupRecycler() {

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
        //val layoutManager : RecyclerView.LayoutManager = GirdLayoutManager(context, 3)

        binding.pokemonRecycler.setLayoutManager(layoutManager)
        binding.pokemonRecycler.adapter = adapter
    }
}