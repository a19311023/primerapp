package com.example.pokemon.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokemon.domain.GetPokemon
import com.example.pokemon.models.Pokemon
import kotlinx.coroutines.launch

class PokemonViewModels : ViewModel(){

    val pokemonModel = MutableLiveData<Pokemon>()

    var getPokemon = GetPokemon()

    fun onCreate(){
        viewModelScope.launch {
            val result = getPokemon()
        }

        //TODO REVISAR QUE PASA SI ES NULO O VACIO. PASAR CAMBIOS A LA IU.
    }
}