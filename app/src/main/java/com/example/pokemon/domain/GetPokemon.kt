package com.example.pokemon.domain

import com.example.pokemon.data.PokemonData

class GetPokemon {

    private val data = PokemonData()

    suspend operator fun invoke() = data.getAllPokemon()

}