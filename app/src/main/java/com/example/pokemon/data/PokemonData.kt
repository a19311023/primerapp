package com.example.pokemon.data

import com.example.pokemon.models.Pokemon
import com.example.pokemon.models.PokemonProvider
import com.example.pokemon.network.PokemonService

class PokemonData {

    private val api = PokemonService()

    suspend fun getAllPokemon(): List<Pokemon>{
        val response= api.getPokemon()
        PokemonProvider.pokemon = response
        return response
    }
}